> adi Wishlist App
App to get data from adidas ecomm api and create own wishlist of products

> Prerequisites
Make sure you have installed node, npm and git

> Getting Started
1. Clone the repositiory in your local machine
2. Go inside the repository of the project and run npm install
3. Run in the terminal npm run dev to start the app

> Others
Note the client run in port 3000. The server run in http://localhost:5000/api/wishlist

> Authors
Nuria Martinez Valladolid

Enjoy! :)
