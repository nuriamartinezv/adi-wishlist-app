const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 5000;
const endpoint = '/api/wishlist';

app.use(bodyParser.json());
app.listen(port, () => console.log(`API:  http://localhost:${port}${endpoint}`));


class ProductList {
  constructor() {
    this.products = []
  }
};

class Product {
  constructor(name, img) {
    this.name = name;
    this.img = img;
  }
}

const wishlist = new ProductList();

app.get(endpoint, (req, res) => {
  res.send(wishlist);
});

app.post(endpoint, (req, res) => {
  const product = new Product(req.body.name, req.body.img);
  wishlist.products.push(product);
  res.send(wishlist);
});

app.delete(endpoint, (req, res) => {
  let productPosition = req.body.position;
  wishlist.products.splice(productPosition, 1);
  res.send(wishlist);
});