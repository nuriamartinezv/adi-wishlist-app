import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ProductListComp from './Components/ProductListComp/ProductListComp';
import "./styles.css";

class ProductList {
  constructor() {
    this.products = []
  }
};

class Product {
  constructor(name, img) {
    this.name = name;
    this.img = img;
  }
}


class App extends Component {
  state = {
    availableProducts: new ProductList(),
    wishlist: new ProductList(),
    searchInput: "",
    buttonState: "is-primary"
  };
  
  componentDidMount = () => {
    fetch('/api/wishlist')
      .then(res => res.json())
      .then(data => {
        this.setState({
          wishlist: data
        })
      }) 
    
  }

  handleInputChange = (event) => {
    let searchInput = event.target.value;
    this.setState({
      searchInput: searchInput
    });
  };
  
  onKeyPress = (e) => {
    if(e.keyCode === 13){
      this.onSubmit();
    }
  }

  onSubmit = () => {
    this.setState({buttonState: "is-loading"});
    let searchInput = this.state.searchInput;
    let availableProducts = this.state.availableProducts;
    availableProducts = new ProductList();
    fetch('https://www.adidas.co.uk/api/suggestions/' + searchInput)
      .then(res => res.json())
      .then(data => {
        data.products.map(product => {
          availableProducts.products.push(new Product(product.suggestion, product.image));
          return availableProducts;
        })
        this.setState({
          availableProducts: availableProducts,
          buttonState: "is-primary"
        })
      })
  };

  addProduct = (product) => {
    fetch('/api/wishlist', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: product.name,
        img: product.img
      })
    })
    fetch('/api/wishlist')
      .then(res => res.json())
      .then(data => {
        this.setState({
          wishlist: data
        })
      }) 
  }

  deleteProduct = (product) => {
    let wishlist = this.state.wishlist;
    let productPosition = wishlist.products.indexOf(product);
    fetch('/api/wishlist', {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        position: productPosition
      })
    })
    fetch('/api/wishlist')
      .then(res => res.json())
      .then(data => {
        this.setState({
          wishlist: data
        })
      }) 
  }

  render() {
    return (
      <Router>
        <div>
          <nav className="navbar is-light">
            <div className="navbar-brand">
              <Link className="navbar-item" to="/">Home</Link>
              <Link className="navbar-item" to="/wishlist">
                <span>wishlist&nbsp;</span>
                <span 
                  className={"tag is-medium " + ((this.state.wishlist.products <= 0) ? "" : "is-primary")}>
                  {this.state.wishlist.products.length}
                </span>
              </Link>
            </div>
          </nav>

          <Route path="/" exact render={() =>
            <div className="section">
              <div className="field has-addons">
                <div className="control">
                  <input 
                    className="input"
                    type="text" 
                    placeholder="write here" 
                    onChange={this.handleInputChange.bind(this)}
                    onKeyDown={this.onKeyPress}
                  />
                </div>
                <div className="control">
                  <button 
                    className={`button ${this.state.buttonState}`}
                    type="button" 
                    onClick={this.onSubmit}
                    >search
                    </button>
                </div>
              </div>

              <div>
                <ProductListComp
                  classNameContainer="columns is-multiline"
                  classNameChild="column is-one-quarter"
                  products={this.state.availableProducts.products}
                  button="+"
                  clicked={this.addProduct}
                />
              </div>
            </div>
          }/>    
          
          <Route path="/wishlist" render={() =>
            <div className="section">
              <h1 className="title">Your wishlist:</h1>
              {(this.state.wishlist.products <= 0) ? 
                <p className="subtitle">Your wishlist is empty, please add some products</p> : 
                <ProductListComp
                  classNameContainer="columns is-multiline"
                  classNameChild="column is-one-quarter"
                  products={this.state.wishlist.products}
                  button="-"
                  clicked={this.deleteProduct}
                />
              }
            </div>
          }/>

        </div>
      </Router>
    );
  }
}


export default App;