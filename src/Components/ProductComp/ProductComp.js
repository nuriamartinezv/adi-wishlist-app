import React from 'react';


const ProductComp = (props) => {
  return (
    <div className="box">
      <img src={props.productContent.img} alt={props.productContent.name} height="100" width="100"/>
      <p>{props.productContent.name}</p>
      <div>
      <button className="button" onClick={props.clicked}>{props.button}</button>
      </div> 
    </div>
  );
}

export default ProductComp;