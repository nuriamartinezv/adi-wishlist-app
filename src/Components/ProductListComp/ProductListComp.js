import React from 'react';
import ProductComp from '../ProductComp/ProductComp';


const ProductListComp = (props) => {
  return (
      <ul className={props.classNameContainer}>
        {props.products.map((product, index) => {
          return (
            <li key={index} className={props.classNameChild}>
              <ProductComp 
                productContent={product}
                button={props.button}
                clicked={props.clicked.bind(this, product)}
                />
            </li>
          )
        })}
      </ul>
  );
}

export default ProductListComp;